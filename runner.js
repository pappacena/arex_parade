#!/usr/bin/env node
var Parade = require("./lib/parade");

function processData(input) {
  console.log(Parade.getSquareOfTime(input));
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
  _input += input;
});

process.stdin.on("end", function () {
  processData(_input);
});
