var assert = require('assert');
var Parade = require('../lib/parade');

describe("Test basic stuff from the Mdina parade algorithm", function() {
  it('should calculate basic eclidean distance right.', function(done) {
    var dist = Parade.distance([0, 1], [100, 1]);
    assert.equal(dist, 100);
    done();
  });

  it('should get the matrix of distances between knights and horses', function(done) {
    var k = [[0, 1], [0, 2], [0, 3]],
        h = [[100, 1], [200, 2], [300, 3]],
        d = Parade.distance;

    var p = new Parade(k, h);
    var m = p.getCostMatrix();

    assert.deepEqual(m, [
      [d(k[0], h[0]), d(k[0], h[1]), d(k[0], h[2])],
      [d(k[1], h[0]), d(k[1], h[1]), d(k[1], h[2])],
      [d(k[2], h[0]), d(k[2], h[1]), d(k[2], h[2])],
    ]);

    done();
  });

  it('should get the min horse-knight distance', function(done) {
    var k = [[0, 1], [0, 2], [0, 3]],
        h = [[100, 1], [200, 2], [300, 3]];

    var p = new Parade(k, h);
    var best = p.getClosestKnightHorsePair();

    assert.deepEqual(best, {
      knightPosition: 0,
      horsePosition: 0
    });

    done();
  });

  it('should get the min horse-knight distance ignoring horse', function(done) {
    var k = [[0, 1], [0, 2], [0, 3]],
        h = [[100, 1], [200, 2], [300, 3]];

    var p = new Parade(k, h);
    var best = p.getClosestKnightHorsePair([], [0]);

    assert.deepEqual(best, {
      knightPosition: 1,
      horsePosition: 1
    });

    done();
  });

  it('should get the min horse-knight distance ignoring knight', function(done) {
    var k = [[0, 1], [0, 2], [0, 3]],
        h = [[100, 1], [200, 2], [300, 3]];

    var p = new Parade(k, h);
    var best = p.getClosestKnightHorsePair([0, 1], []);

    assert.deepEqual(best, {
      knightPosition: 2,
      horsePosition: 0
    });

    done();
  });

  it('should get the right knights for the assignment', function(done) {
    var k = [[0, 1], [0, 2], [0, 3]],
        h = [[100, 1], [200, 2], [300, 3]];

    var p = new Parade(k, h);
    var knightHorse = p.select(2);

    assert.deepEqual(knightHorse, {
      0: 0,
      1: 1
    });

    done();
  });

  it('should get the total time of the assignment example', function(done) {
    var k = [[0, 1], [0, 2], [0, 3]],
        h = [[100, 1], [200, 2], [300, 3]];

    var p = new Parade(k, h);
    var knightHorse = {
      0: 0,
      1: 1
    };

    assert.equal(p.getTotalTime(knightHorse), 200);

    done();
  });


  it('should build parade parameters from string', function(done) {
    var input = "3 3 2\n" +
                "0 1\n" +
                "0 2\n" +
                "0 3\n" +
                "100 1\n" +
                "200 2\n" +
                "300 3\n";
    var ret = Parade.fromString(input);

    assert.deepEqual(ret.parade.knights, [[0, 1], [0, 2], [0, 3]]);
    assert.deepEqual(ret.parade.horses, [[100, 1], [200, 2], [300, 3]]);
    assert.equal(ret.maxKnights, 2);

    done();
  });

  it('should build parade parameters from string', function(done) {
    var input = "3 3 2\n" +
                "0 1\n" +
                "0 2\n" +
                "0 3\n" +
                "100 1\n" +
                "200 2\n" +
                "300 3\n";

    assert.equal(Parade.getSquareOfTime(input), 40000);
    done();
  });

});
