function Parade(knights, horses) {
  this.knights = knights;
  this.horses = horses;
  this.matrix = undefined;

  /**
   * Get the distance matrix between knights and horses
   */
  this.getCostMatrix = function() {
    var matrix = [],
        knights = this.knights,
        horses = this.horses;
    for(var k=0; k<knights.length; k++) {
      matrix[k] = [];
      for(var h=0; h<horses.length; h++) {
        matrix[k][h] = Parade.distance(knights[k], horses[h]);
      }
    }
    return matrix;
  };

  /**
   * Gets the closest knight-horse pair
   */
  this.getClosestKnightHorsePair = function(ignoreKnights, ignoreHorses) {
    var ignoreKnights = ignoreKnights || [],
        ignoreHorses = ignoreHorses || [],
        min = undefined,
        minPosition = {horsePosition: undefined, knightPosition: undefined};

    if(this.matrix === undefined) {
      this.matrix = this.getCostMatrix();
    }

    this.matrix.forEach(function(row, k) {
      if(ignoreKnights.indexOf(k) !== -1) {
        return;
      }
      row.forEach(function(value, h) {
        if(ignoreHorses.indexOf(h) !== -1) {
          return;
        }
        if(min === undefined || value < min) {
          min = value;
          minPosition.horsePosition = h;
          minPosition.knightPosition = k;
        }
      })
    });

    return minPosition;
  };

  /**
   * Select the n knights closer to horses
   */
  this.select = function(nKnights) {
    var knightHorse = {},
        selectedKnights = [],
        selectedHorses = [],
        pair;

    while(selectedKnights.length < nKnights) {
      pair = this.getClosestKnightHorsePair(selectedKnights, selectedHorses);
      knightHorse[pair.knightPosition] = pair.horsePosition;

      selectedKnights = Object.keys(knightHorse);
      selectedHorses = selectedKnights.map(function(k) {
        return knightHorse[k];
      });
    }

    return knightHorse;
  };

  /**
   * Returns the total time
   */
  this.getTotalTime = function(knightHorsePairs) {
    if(this.matrix === undefined) {
      this.matrix = this.getCostMatrix();
    }
    var matrix = this.matrix;

    var max = 0;
    Object.keys(knightHorsePairs).forEach(function(knightPosition) {
      var horsePosition = knightHorsePairs[knightPosition];
      var time = matrix[knightPosition][horsePosition];
      if(time > max) {
        max = time;
      }
    })
    return max;
  };

  return this;
}


/**
 * Gets the distance between a and b
 */
Parade.distance = function(a, b) {
  var x = Math.pow(a[0] - b[0], 2) + Math.pow(a[1] - b[1], 2);
  return Math.sqrt(x);
};

function strToPosition(str) {
  var x = str.split(' ');
  return [
    parseInt(x[0], 10),
    parseInt(x[1], 10)
  ];
}

/**
 * Gets parameters for the problem from input string
 */
Parade.fromString = function(input) {
  var lines = input.split("\n");
  var numbers = lines[0].split(" ");
  var totalKnights = parseInt(numbers[0], 10),
      totalHorses = parseInt(numbers[1], 10),
      maxKnights = parseInt(numbers[2], 10);

  var knightPositions = [],
      horsePositions = [],
      currentLine = 1;

  if(totalKnights > 250 || totalHorses > 250) {
    throw new Error("You cannot put more than 250 horses or knights");
  }
  if(maxKnights > totalKnights || maxKnights > totalHorses) {
    throw new Error('We dont have so many knights or horses');
  }

  // read knights positions
  for(var i=currentLine; i<currentLine + totalKnights; i++) {
    knightPositions.push(strToPosition(lines[i]));
  }
  currentLine += totalKnights;

  // read horses positions
  for(var i=currentLine; i<currentLine + totalHorses; i++) {
    horsePositions.push(strToPosition(lines[i]));
  }

  return {
    parade: new Parade(knightPositions, horsePositions),
    maxKnights: maxKnights
  }
};

/**
 * Solve the problem
 */
Parade.getSquareOfTime = function(input) {
  var params = Parade.fromString(input);
  var parade = params.parade,
      maxKnights = params.maxKnights;

  var selected = parade.select(maxKnights);
  return Math.pow(parade.getTotalTime(selected), 2);
};

module.exports = Parade;
