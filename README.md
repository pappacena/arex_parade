Instructions
============

* Run `npm install` before anything
* To run the test suit, just run `npm test`
* To execute the assignment script, run `npm run-script consolerun`
* You can also generate a code coverage report using `npm run-script coverage`
